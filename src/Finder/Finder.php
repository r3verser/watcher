<?php
/**
 * Finder component
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Finder;

use Finder\Filter\ExtensionFilter;
use Finder\Filter\FilesFilter;
use Finder\Filter\SizeFilter;

/**
 * Class Finder
 * @package Finder
 */
class Finder implements \IteratorAggregate
{
    /**
     * @var array Path where to find
     */
    private $paths = [];
    /**
     * @var bool Search only files
     */
    private $filesOnly = false;
    /**
     * @var array
     */
    private $sizes = [];
    /**
     * @var array
     */
    private $extensions = [];

    /**
     * Search only files
     * @param bool $status
     * @return $this
     */
    public function files($status = true)
    {
        $this->filesOnly = $status;
        return $this;
    }

    /**
     * Usage example:
     *
     * size('> 100K')
     * size('<= 250B')
     *
     * @param $string size logic string
     * @return $this
     */
    public function size($string)
    {
        $this->sizes[] = $string;
        return $this;
    }

    /**
     * @param array $ext
     */
    public function ext(array $ext = [])
    {
        $this->extensions = $ext;
    }

    /**
     * Search in path
     * @param $path
     * @return $this
     */
    public function in($path)
    {
        $this->paths[] = $path;
        return $this;
    }

    /**
     * @return \RecursiveIteratorIterator
     */
    public function getIterator()
    {
        return $this->find();
    }

    /**
     * Apply's all selected filters and returns result
     * @return \AppendIterator
     */
    protected function find()
    {
        $pathsIterators = new \AppendIterator();

        foreach ($this->paths as $path) {
            $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
            $pathsIterators->append($iterator);
        }

        if ($this->filesOnly) {
            $pathsIterators = new FilesFilter($pathsIterators);
        }

        if (count($this->sizes)) {
            foreach ($this->sizes as $size) {
                $pathsIterators = new SizeFilter($pathsIterators, $size);
            }
        }

        if (count($this->extensions)) {
            $pathsIterators = new ExtensionFilter($pathsIterators, $this->extensions);
        }

        return $pathsIterators;
    }
}

