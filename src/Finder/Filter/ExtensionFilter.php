<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Finder\Filter;

/**
 * Search only files
 * Class FilesFilter
 */
class ExtensionFilter extends \FilterIterator
{
    protected $extensions = [];
    /**
     * {@inheritdoc}
     */
    public function __construct(\Iterator $iterator, array $extensions)
    {
        parent::__construct($iterator);
        $this->extensions = $extensions;
    }

    /**
     * {@inheritdoc}
     */
    public function accept()
    {
        if (!$this->current()->isDir() && !in_array($this->current()->getExtension(), $this->extensions)) {
            return false;
        }
        return true;
    }
}