<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Finder\Filter;

use Finder\Filter\Exception\NotImplementedException;
use InvalidArgumentException;

/**
 * Class SizeFilter
 * @package Finder\Filter
 */
class SizeFilter extends \FilterIterator
{
    /**
     * @var string
     */
    private $logicExpr;
    /**
     * @var int
     */
    private $sizeNumber;
    /**
     * @var string
     */
    private $sizeUnit;
    /**
     * @var array
     */
    private $allowedLogicExpr = [
        '>',
        '<',
        '>=',
        '<=',
        '=',
        '!=',
    ];

    /**
     * @param \Iterator $iterator
     * @param string    $sizeString
     * @throws \InvalidArgumentException
     */
    public function __construct(\Iterator $iterator, $sizeString)
    {
        parent::__construct($iterator);

        if (!preg_match('/([><=\!]+)(|\s+)([\d]+)(B|K|M|G)/i', $sizeString, $matches)) {
            throw new \InvalidArgumentException($sizeString);
        }

        $this->logicExpr = $matches[1];
        $this->sizeNumber = $this->convertToBytes($matches[3], $matches[4]);
        $this->sizeUnit = $matches[4];
    }

    /**
     * Converts number to bytes
     * @param $digit
     * @param $unit Size unit
     * @return int Size in bytes
     */
    protected function convertToBytes($digit, $unit)
    {
        switch (strtoupper($unit)) {
            case 'G':
            case 'GB':
                $digit *= 1073741824;
                break;
            case 'M':
            case 'MB':
                $digit *= 1048576;
                break;
            case 'K':
            case 'KB':
                $digit *= 125;
                break;
            default:
                break;
        }

        return $digit;
    }

    /**
     * {@inheritdoc}
     */
    public function accept()
    {
        if (!$this->current()->isFile()) {
            return true;
        }
        return $this->testLogic($this->current()->getSize(), $this->sizeNumber, $this->logicExpr);
    }

    /**
     * Tests logic expression
     * @param int    $currentValue
     * @param int    $neededValue
     * @param string $logicExpr
     * @return bool
     * @throws \LogicException
     * @throws Exception\NotImplementedException
     */
    protected function testLogic($currentValue, $neededValue, $logicExpr)
    {
        if (!in_array($logicExpr, $this->allowedLogicExpr)) {
            throw new \LogicException($logicExpr);
        }

        switch ($logicExpr) {
            case '>':
                return ($currentValue > $neededValue);
            case '<':
                return ($currentValue < $neededValue);
            case '>=':
                return ($currentValue >= $neededValue);
            case '<=':
                return ($currentValue <= $neededValue);
            case '=':
                return ($currentValue == $neededValue);
            case '!=':
                return ($currentValue <> $neededValue);
            default:
                throw new NotImplementedException($logicExpr);
        }
    }
}