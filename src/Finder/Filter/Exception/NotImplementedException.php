<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Finder\Filter\Exception;

class NotImplementedException extends \Exception
{

}