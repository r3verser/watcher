<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Finder\Filter;

/**
 * Search only files
 * Class FilesFilter
 */
class FilesFilter extends \FilterIterator
{
    /**
     * {@inheritdoc}
     */
    public function __construct(\Iterator $iterator)
    {
        parent::__construct($iterator);
    }

    /**
     * {@inheritdoc}
     */
    public function accept()
    {
        if ($this->current()->isDir()) {
            return false;
        }
        return true;
    }
}