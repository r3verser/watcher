<?php
/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Event;

/**
 * Default event object
 * Class Event
 * @package Event
 */
class Event
{
    /**
     * @var null|object
     */
    protected $context;
    /**
     * @var mixed
     */
    protected $params;

    /**
     * Constructor
     *
     * @param       $context
     * @param array $params
     * @internal param object $name
     */
    public function __construct($context = null, $params = null)
    {
        $this->context = $context;
        $this->params = $params;
    }

    /**
     * Returns context of triggered event
     *
     * @return null|object
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Returns params sended with event
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}