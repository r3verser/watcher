<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Event;

/**
 * Class EventManager
 * @package Event
 */
class EventManager
{
    /**
     * @var array
     */
    protected $subscribers = [];
    /**
     * @var null|EventManager
     */
    protected static $instance = null;
    /**
     * @var bool
     */
    protected $propagate = true;

    private function __construct(){}

    private function  __clone(){}

    /**
     * @return EventManager
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * Dispatches events
     * @param       $eventName
     * @param Event $event
     */
    public function dispatch($eventName, Event $event)
    {
        if (isset($this->subscribers[$eventName]) && count($this->subscribers[$eventName])) {
            foreach ($this->subscribers[$eventName] as $subscriber) {
                if (is_callable($subscriber)) {
                    call_user_func($subscriber, $event);
                }
                if ($this->propagate === false) {
                    break;
                }
            }
        }
    }

    /**
     * Adds subscriber to event
     * @param $event
     * @param $subscriber
     */
    public function addSubscriber($event, $subscriber)
    {
        $this->subscribers[$this->getEventName($event)][] = $subscriber;
    }

    /**
     * Returns name of the event
     * @param $event
     * @return string
     */
    protected function getEventName($event)
    {
        $event = (is_object($event) && $event instanceof Event) ? $event->getName() : $event;
        return $event;
    }

    public function stopPropagation()
    {
        $this->propagate = false;
    }
}