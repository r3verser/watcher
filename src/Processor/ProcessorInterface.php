<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
 namespace Watcher\Processor;

 /**
  * Interface ProcessorInterface
  * @package Watcher\Processor
  */
 interface ProcessorInterface
 {
     /**
      * Processes file with custom algorithm
      *
      * @param $file
      * @return mixed
      */
     public function process($file);

 }