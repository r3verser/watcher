<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Watcher\Snapshot;

use Watcher\Snapshot\Provider\ProviderInterface;

/**
 * Class Snapshot
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class Snapshot extends SnapshotAbstract
{
    /**
     * Constructor
     *
     * @param string            $name
     * @param ProviderInterface $provider
     */
    public function __construct($name, ProviderInterface $provider)
    {
        $this->name = $this->buildName($name);
        $this->provider = $provider;
        $this->data = $provider->getData();
    }

    /**
     * Returns name of the snapshot
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Builds name of the snapshot
     *
     * @param string $name
     * @return string
     */
    protected function buildName($name)
    {
        return md5($name) . '.watcher';
    }

    /**
     * Returns snapshot data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

}