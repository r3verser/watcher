<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Watcher\Snapshot\Provider;

/**
 * Class IteratorProvider
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class FilesIteratorProvider implements ProviderInterface
{
    /**
     * @var \IteratorAggregate Finder instance
     */
    protected $iterator;

    /**
     * Constructor
     *
     * @param \IteratorAggregate $iterator
     */
    public function __construct(\IteratorAggregate $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * Builds and returns data about files from Finder
     *
     * @return array
     */
    public function getData()
    {
        $data = [];
        foreach ($this->iterator as $file) {
            $data[$file->getRealpath()] = hash_file('md5', $file->getRealpath());
        }
        return $data;
    }

}