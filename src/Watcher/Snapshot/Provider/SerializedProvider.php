<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Watcher\Snapshot\Provider;

/**
 * Class SerializedProvider
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class SerializedProvider implements ProviderInterface
{
    /**
     * @var Finder
     */
    protected $data;

    /**
     * Constructor
     *
     * @param string $serializedData
     */
    public function __construct($serializedData)
    {
        $this->data = $serializedData;
    }

    /**
     * Returns unserialized data from loaded snapshot
     *
     * @return array
     */
    public function getData()
    {
        return unserialize($this->data);
    }

}