<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Watcher\Snapshot\Provider;

/**
 * Interface ProviderInterface
 * @package Watcher\Snapshot\Provider
 */
interface ProviderInterface
{
    /**
     * Returns data about tracked files
     *
     * @return array
     */
    public function getData();

}