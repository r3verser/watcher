<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Watcher\Snapshot;

/**
 * Class SnapshotAbstract
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
abstract class SnapshotAbstract implements SnapshotInerface, \ArrayAccess, \Iterator
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var ProviderInterface
     */
    protected $provider;
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return null
     */
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }


    /**
     * @param mixed $offset
     * @param mixed $value
     * @throws NotImplementedException
     */
    public function offsetSet($offset, $value)
    {
        throw new NotImplementedException();
    }


    /**
     * @param mixed $offset
     * @throws NotImplementedException
     */
    public function offsetUnset($offset)
    {
        throw new NotImplementedException();
    }

    /**
     *
     */
    public function rewind()
    {
        reset($this->data);
    }

    /**
     * @return mixed
     */
    public function current()
    {
        $var = current($this->data);
        return $var;
    }

    /**
     * @return mixed
     */
    public function key()
    {
        $var = key($this->data);
        return $var;
    }

    /**
     * @return mixed
     */
    public function next()
    {
        $var = next($this->data);
        return $var;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $key = key($this->data);
        $var = ($key !== null && $key !== false);
        return $var;
    }
}
  