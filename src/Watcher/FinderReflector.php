<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Watcher;

use Symfony\Component\Finder\Finder;

/**
 * Class FinderReflector
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class FinderReflector
{
    /**
     * @var null
     */
    protected $object = null;
    /**
     * @var null|\ReflectionObject
     */
    protected $reflectedObject = null;

    /**
     * @param $finder
     */
    public function __construct(Finder $finder)
    {
        $this->object = $finder;
        $this->reflectedObject = new \ReflectionObject($finder);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        $property = $this->reflectedObject->getProperty($name);
        $property->setAccessible(true);
        $value = $property->getValue($this->object);
        $property->setAccessible(false);

        return $value;
    }

}