<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
namespace Watcher;

use Event\Event;
use Event\EventManager;
use League\Flysystem\FilesystemInterface;
use Watcher\Exception\NotImplementedException;
use Watcher\Snapshot\Provider\FilesIteratorProvider;
use Watcher\Snapshot\Provider\SerializedProvider;
use Watcher\Snapshot\Snapshot;
use Watcher\Snapshot\SnapshotAbstract;

/**
 * Class Watcher
 * @package Watcher
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class Watcher
{
    /**
     * Event names
     */
    const
        EVENT_FILE_CREATED = 'watcher.file.created',
        EVENT_FILE_CHANGED = 'watcher.file.changed',
        EVENT_FILE_DELETED = 'watcher.file.deleted',
        EVENT_SNAPSHOT_SAVED = 'watcher.snapshot.saved',
        EVENT_SNAPSHOT_DELETED = 'watcher.snapshot.deleted';
    /**
     * @var FilesystemInterface
     */
    protected $storage;

    /**
     * Constructor
     *
     * @param FilesystemInterface $storage
     */
    public function __construct(FilesystemInterface $storage = null)
    {
        $this->storage = $storage;
    }

    /**
     * Sets storage for snapshot's (local, ftp, s3, dropbox etc.)
     *
     * @param FilesystemInterface $storage
     */
    public function setStorage(FilesystemInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Returns Storage object
     *
     * @return FilesystemInterface
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Compares snapshots and triggers events if files were changed, created or deleted
     *
     * @param SnapshotAbstract $localSnapshot
     * @param SnapshotAbstract $remoteSnapshot
     */
    public function compare(SnapshotAbstract $localSnapshot, SnapshotAbstract $remoteSnapshot)
    {
        foreach ($localSnapshot as $filePath => $fileHash) {
            if (!isset($remoteSnapshot[$filePath])) {
                EventManager::getInstance()->dispatch(self::EVENT_FILE_CREATED, new Event($this, ['filepath' => $filePath]));
                continue;
            }

            if ($fileHash !== $remoteSnapshot[$filePath]) {
                EventManager::getInstance()->dispatch(self::EVENT_FILE_CHANGED, new Event($this, ['filepath' => $filePath]));
            }
        }

        foreach ($remoteSnapshot as $filePath => $fileHash) {
            if (!isset($localSnapshot[$filePath])) {
                EventManager::getInstance()->dispatch(self::EVENT_FILE_DELETED, new Event($this, ['filepath' => $filePath]));
            }
        }
    }

    /**
     * Updates snapshot
     *
     * @param SnapshotAbstract $snapshot
     * @throws NotImplementedException
     */
    public function update(SnapshotAbstract $snapshot)
    {
        throw new NotImplementedException();
    }

    /**
     * Removes snapshot
     *
     * @param SnapshotAbstract $snapshot
     * @throws NotImplementedException
     */
    public function delete(SnapshotAbstract $snapshot)
    {
        throw new NotImplementedException();
    }

    /**
     * Tracks filesystem for changing
     * Creates snapshot of selected path and saves it to remote storage
     *
     * @param \IteratorAggregate $filesIterator
     * @param string $snapshotName
     */
    public function track(\IteratorAggregate $filesIterator, $snapshotName)
    {
        $localSnapshot = new Snapshot($snapshotName, new FilesIteratorProvider($filesIterator));

        if (!$this->storage->has($localSnapshot->getName())) {
            if ($this->storage->put($localSnapshot->getName(), serialize($localSnapshot->getData()))) {
                EventManager::getInstance()->dispatch('watcher.snapshot.saved', new Event($localSnapshot));
                return;
            }
        }
        $this->compare($localSnapshot, new Snapshot($snapshotName, new SerializedProvider($this->storage->read($localSnapshot->getName()))));
    }

}