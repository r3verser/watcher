<?php

/*
 * This file is part of the test.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
  namespace Watcher\Exception;

  /**
   * Class NotImplementedException
   * @package Watcher
   * @author  Artem Voitko <r3verser@gmail.com>
   */
  class NotImplementedException extends \Exception
  {

  }