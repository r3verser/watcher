<?php
/**
 *
 * @author Artem Voitko <r3verser@gmail.com>
 */
require_once 'vendor/autoload.php';


/**
 * Subscribing to Watcher events
 */
\Event\EventManager::getInstance()->addSubscriber(\Watcher\Watcher::EVENT_FILE_CREATED, function ($event) {
    echo "New file: {$event->getParams()['filepath']}\r\n";
});

\Event\EventManager::getInstance()->addSubscriber(\Watcher\Watcher::EVENT_FILE_DELETED, function ($event) {
    echo "Deleted: {$event->getParams()['filepath']}\r\n";
});

\Event\EventManager::getInstance()->addSubscriber(\Watcher\Watcher::EVENT_FILE_CHANGED, function ($event) {
    echo "Changed: {$event->getParams()['filepath']}\r\n";
});


/**
 * Creating and running
 */
$files = \Symfony\Component\Finder\Finder::create()->in(__DIR__)->files('*.php');
$watcher = new \Watcher\Watcher();
$watcher->setStorage(new \League\Flysystem\Filesystem(new \League\Flysystem\Adapter\Local(__DIR__)));
$watcher->track($files, 'uniquename');





















